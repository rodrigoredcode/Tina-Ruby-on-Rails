class CreateEventos < ActiveRecord::Migration
  def change
    create_table :eventos do |t|
      t.string :titulo
      t.string :tipo
      t.date :data
      t.string :local
      t.text :descricao

      t.timestamps null: false
    end
  end
end
