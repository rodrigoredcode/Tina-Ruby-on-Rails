class EventosController < ApplicationController
  def index
  end

  def show
      @evento = Evento.find(params[:id])
  end

  def new
    @evento = Evento.new
  end

  def create
    @evento = Evento.new(evento_params)
    if @evento.save
      redirect_to @evento
    else
      render 'new'
    end
  end

  private

  def evento_params
    params.require(:evento).permit(:titulo, :tipo, :data, :local, :descricao)
  end
end
